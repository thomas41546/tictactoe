import random

from Agent import Agent
from Board import Board

from operator import itemgetter
import random

node_data_lookup = {}

class Node:

    children = None
    value = None
    
    data = None
    
    
    def __init__(self,data):
        self.data = data
        self.children = []
        self.value = []

    def isLeaf(self):
        return bool(len(self.children) == 0)

    def addNode(self, node, value):
        if node not in self.children:
            self.children += [node]
            self.value += [value]
            #global node_data_lookup
            #node_data_lookup[self.data,node.data] = node

    def hasData(self,data):
        return bool(self.data == data)
    
    #untested
    def setChildValue(self, child, value):
        for i in range(0,len(self.children)):
            if self.children[i] == child:
                self.value[i] += value
                return

    def getNodeWithData(self, data):
        #global node_data_lookup
        #try:
        #    return node_data_lookup[self.data,data]
        #except:
        #    pass
        ret = self.getNodeWithDataRecur(data)
        node_data_lookup[self, data] = ret
        return ret

    def getNodeWithDataRecur(self, data):
        if self.hasData(data):
            return self
        else:
            for child in self.children:
                ret = child.getNodeWithDataRecur(data)
                if ret is not None:
                    return ret
        return None

    def __str__(self):
        return self.layerstr(0,0)
        

    def layerstr(self,value,depth):
        out =  "\t" * depth  + "["+ str(value) + "]"+ str(self.data) + "\n"
        if not self.isLeaf():
            for i in range(0,len(self.children)):
                child = self.children[i]
                value = self.value[i]
                out +=  (child.layerstr(value,depth+1))+"\n"
            out = out[:-1]
        return out
    
    def printSelf(self):
        print (self.data)

    def maxNodePath(self,value):
        if self.isLeaf():
            return value

        rets = []
        for i in range(0,len(self.children)):
            child = self.children[i]
            value = self.value[i]
            rets += [child.maxNodePath(value)]

        return max(rets)
    
    def findNextBestNodes(self):        
        best_nodes = []
        
        for i in range(0,len(self.children)):
            child = self.children[i]

            ret = child.maxNodePath(self.value[i])
            if ret > 0:
                return [(self.children[i].data,ret)]
            elif ret == 0:
                best_nodes += [(self.children[i].data,ret)]
        
        if len(best_nodes) <= 0:
            return None
        else:
            return best_nodes


def buildNodeTree(root, trav, val):
    lasttrav = trav[-1]
    cur = root
    for entry in trav:
        next = cur.getNodeWithData(entry)
        
        # traverse until we find an entry does not exist in tree
        if next is not None:
            if entry == lasttrav:
                cur.setChildValue(entry,val)
            
            cur = next
        else:
            # last item in trav is important because it has value
            newnode = Node(entry)
            #print "added node"
            if entry == lasttrav:
                cur.addNode(newnode,val)
            else:
                cur.addNode(newnode,0)
            cur = newnode

def buildNodeTree(root, trav, val):
    lasttrav = trav[-1]
    cur = root
    for entry in trav:
        next = cur.getNodeWithData(entry)
        
        # traverse until we find an entry does not exist in tree
        if next is not None:
            if entry == lasttrav:
                next.setChildValue(entry,val)
            
            cur = next
        else:
            # last item in trav is important because it has value
            newnode = Node(entry)
            #print "added node"
            if entry == lasttrav:
                cur.addNode(newnode,val)
            else:
                cur.addNode(newnode,0)
            cur = newnode

def demo():
    things = [([1,2,3], 0),
              ([1,2,4], -1),
              ([1,6,7], 0),
              ([1,5,3], 0),
              ([1,5,8,9],1)]

    root = Node(1)

    for t in things:
        trav,val = t
        buildNodeTree(root,trav,val)

    print (root)
    for node,val in root.findNextBestNodes():
        print ("node/%s val/%d" % (node,val))

#demo();

##############################################################################
class XanaxAgent(Agent):

    trans = []
    
    zero_board = Board(board_state = [[0]*3 for x in range(0,3)])
    
    root_node = Node(zero_board)
    
    hitSuccess = 0
    hitCount = 0
    lostCount = 0
    lostSum = 0
    
    verbose = False
    debugi = 0

    def move(self, board):

        cur = self.root_node.getNodeWithData(board)
        
        self.hitCount += 1
        
        bestBoards = None
        if cur is not None:
            bestBoards = cur.findNextBestNodes()
        
        if bestBoards is not None:
            
            for boards in bestBoards:
                b,v = boards
                for i in range(0,3):
                    for j in range(0,3):
                        if v >= 0:
                            if board.state[i][j] != b.state[i][j]:
                                self.hitSuccess += 1
                                if self.verbose: print ("found optimal", v)
                                return (i,j)
    
        if self.verbose: print ("did not find a good move")
        spots = []
        for i in range(0,3):
            for j in range(0,3):
                if board.state[i][j] == 0:
                    spots += [(i,j)]
    
        if len(spots) == 0:
            print ("CANNOT MAKE MOVE")

        random.shuffle(spots)
        return spots[0]

    def get_stats(self):
        if self.hitCount != 0:
            print ("XANAX hitrate/%2.3f lost/%d" % (
                    float(self.hitSuccess)/self.hitCount,
                    self.lostCount))
            self.hitSuccess = 0
            self.hitCount = 0
            self.lostCount = 0
    
    def get_name(self):
        return "Xanax"

    def result(self, oldboard, newboard, reward):
        self.trans += [oldboard, newboard]

    def end_game(self, board, reward):
        #   if self.hitCount > 0:
        if self.verbose: print ("hit rate",(float(self.hitSuccess)/self.hitCount))
        if len(self.trans) <= 1:
            self.trans = []
            return

        if self.trans[0] != self.zero_board:
            self.trans = [self.zero_board] + self.trans
                
        if self.trans[-1] != board:
            #print "added last board"
            self.trans += [board]
        
        if reward < 0:
            self.lostCount += 1
            self.lostSum += 1
            if self.lostSum > 200:
                dat = None
                for i in range(0,len(self.trans)-2):
                    dat = self.root_node.getNodeWithData(self.trans[i])
                #if dat != None:
                    #print dat
            #if self.lostSum > 300:
            #    import sys
            #    sys.exit(0)
    
                
        
        buildNodeTree(self.root_node, self.trans, reward)
        buildNodeTree(self.root_node, [x.inverse() for x in self.trans], -1*reward)
        if self.verbose: print (self.root_node)
        self.trans = []


