import random

from Agent import Agent

class RandomAgent(Agent):
    
    def move(self, board):
        spots = []
        for i in range(0,3):
            for j in range(0,3):
                if board.state[i][j] == 0:
                    spots += [(i,j)]
        
        random.shuffle(spots)
        return spots[0]
    
    def get_name(self):
        return "Random-agent"

    def result(self, oldboard, newboard, reward):
        pass

    def new_game(self):
        pass

    def end_game(self, board, reward):
        pass
