from time import time
import random
import pickle

from Board import Board

class Agent:
    
    id = 1
     
    def __init__(self, id):
        if id not in [1,2]: raise BaseException("Your agent must have an id of 1 or 2")
        self.id = int(id)
        pass
     
    # OVERWRIDE
    # IS USED FOR SAVING FILES (no spaces allowed, etc)
    def get_name(self):
        return "Default-agent"
    
    # OVERWRIDE
    # AI is told that the game has ended and a new board is present
    def new_game(self):
        #print "New game has begun for me agent",self.id
        pass

    # OVERRIDE
    # AI is provided the final board state and the reward at end game
    def end_game(self, finalboard, reward):
        #print "Agent %s got reward %d" % (self.id,reward)
        pass
    
    # OVERWRIDE
    # AI selectes the index to place a piece
    # AI is provided with the current board state
    def move(self, board):
        for i in range(0,3):
            for j in range(0,3):
                if board.state[i][j] == 0:
                    # note this is a tuple, not an array
                    return (i,j)

    # OVERWRIDE
    # AI is given this feedback after a move is made
    def result(self, oldboard, newboard, reward):
        #print "Agent %d got reward %d" %(self.id,reward)
        pass

    def save(self,filename):
        output = open(filename, 'wb')
        pickle.dump(self, output)
        output.close()
        print ("Saved agent to file %s" % (filename))
