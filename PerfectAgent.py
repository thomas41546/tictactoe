import random

from Agent import Agent

class PerfectAgent(Agent):

    def move(self, board):
        
        winways = (# Horizontal
                     set([1, 2, 3]),
                     set([4, 5, 6]),
                     set([7, 8, 9]),
                     # Verticle
                     set([1, 4, 7]),
                     set([2, 5, 8]),
                     set([3, 6, 9]),
                     # Diagonal
                     set([1, 5, 9]),
                     set([3, 5, 7]))
        
        cur = []
        for i in range(0,3):
            for j in range(0,3):
                if board.state[i][j] == self.id:
                    cur += [(i)*3 + (j+1)]
        cur = set(cur)
                        
        for win in winways:
            if len(cur.intersection(win)) == 2:
                diff = win - cur 
                for i in range(0,3):
                    for j in range(0,3):
                        if set([i*3 + j+1]) == diff:
                            if board.state[i][j] == 0:
                                return (i,j)


        spots = []
        for i in range(0,3):
            for j in range(0,3):
                if board.state[i][j] == 0:
                    spots += [(i,j)]
        
        random.shuffle(spots)
        return spots[0]
    
    def get_name(self):
        return "Perfect-agent"

    def result(self, oldboard, newboard, reward):
        pass

    def new_game(self):
        pass

    def end_game(self, board, reward):
        pass
