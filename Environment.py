#!/usr/bin/env python
import random
import pickle
from time import time

# local imports
from Board import Board
from Agent import Agent

# custom agents
from XanaxAgent import XanaxAgent
from UserAgent import UserAgent
from RandomAgent import RandomAgent
from PerfectAgent import PerfectAgent
from OnelineAgent import OnelineAgent

class Stat:
    wins = 0
    ties = 0
    def __init__(self, name):
        self.name = name

class Environment: 
    def __init__(self, agent1, agent2):
        self.board = Board()
        
        self.stats = [Stat(agent1.get_name()),Stat(agent2.get_name())]

        if agent1.id == agent2.id:
            raise BaseException("Agents cannot have the same id")
        self.agents = [agent1, agent2]
        self.next = 0
    
        self.games_played = 0
    
        for agent in self.agents:
            agent.new_game()
    
    def runGame(self, printBoard=False):
        lastPlayed = self.games_played
        while True:
            self.runIteration(printBoard=printBoard)
            if self.games_played != lastPlayed:
                break

    def runIteration(self, printBoard=False):
        cur_agent = self.agents[self.next]
        
        agent_move = cur_agent.move(self.board)
        
        if agent_move[0] not in [0,1,2] or \
           agent_move[1] not in [0,1,2] or \
           self.board.state[agent_move[0]][agent_move[1]] != 0:
            raise BaseException("Agent %d tried to place a piece"
                                 " in an invalid location %s" 
                                 % (cur_agent.id, str(agent_move)))
        
        oldboard = Board(board_state = self.board.state)
        self.board.state[agent_move[0]][agent_move[1]] = cur_agent.id
        newboard = Board(board_state = self.board.state)

        # board is full check
        isFull = True
        for i in range(0,3):
            for j in range(0,3):
                if self.board.state[i][j] == 0:
                    isFull = False
                    break

        agent_reward = newboard.score(cur_agent.id)
                        
        cur_agent.result(oldboard,newboard, agent_reward)
    
        if printBoard:
            print(newboard)

        if isFull:
            self.games_played += 1
            
            for i in range(0,2):
                self.stats[i].ties += 1

            for agent in self.agents:
                agent.end_game(self.board,0)
                agent.new_game()            
            self.board = Board()
            
            if printBoard: print ('-'*10)

        elif agent_reward != 0:
            
            self.games_played += 1

            if agent_reward > 0:
                self.stats[self.next].wins += 1
            else:
                if self.next == 0:
                    self.stats[1].wins += 1
                else:
                    self.stats[0].wins += 1

            for agent in self.agents:
                agent.end_game(self.board,self.board.score(agent.id))
                agent.new_game()

            self.board = Board()

            if printBoard: print ('-'*10)
        
        # alternate between agents
        if self.next == 1:
            self.next = 0
        else:
            self.next = 1 

    def statistics(self):
        print ("Overall statistics")
        for stat in self.stats:
            print ("%s" % (stat.name))
            print ("\t> wins/%d ties/%d" %(stat.wins,stat.ties))
            print ("")

    def saveAgents(self):
        # NOTE: 2 agents of the same type will only have the later saved
        for agent in self.agents:
            agent.save()


# you can use this load the state of an existing AI agent
def loadAgent(filepath):
    pkl_file = open(filepath, 'rb')
    agent = pickle.load(pkl_file)
    pkl_file.close()
    print ("Loaded agent from file %s" % (filepath))
    return agent


a1 = XanaxAgent(1)
a2 = RandomAgent(2)
#a2 = RandomAgent(2)
#a2 = loadAgent('Xanax.pkl')

env = Environment(a1,a2)

for j in range(0,1000): 
    for i in range(0,200): 
        env.runGame()
    
    a1.get_stats()


env.statistics()

print ("Round 2 fight")

a2 = UserAgent(2)
env = Environment(a1,a2)

#for i in range(0,10):
#    env.runGame()

env.statistics()

# serializes all agents in environemnt
#env.saveAgents()

# saves a specific agent to a file
#a2.save('Xanax.pkl')

