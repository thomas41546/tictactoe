import random
import pickle
from time import time


class Board:
    state = None
 
    def __init__(self, board_state = None):
        self.state = [[0]*3 for x in range(0,3)]
        
        if board_state == None: return
        
        for i in range(0,3):
            for j in range(0,3):
                self.state[i][j] = board_state[i][j]

    def inverse(self):
        newboard = Board(board_state = self.state)
        for i in range(0,3):
            for j in range(0,3):
                if newboard.state[i][j] == 1: newboard.state[i][j] = 2
                elif newboard.state[i][j] == 2:newboard.state[i][j] = 1
        return newboard

    def __str__(self):
        out = ""
        for i in range(0, len(self.state)):
            out += ','.join([str(x) for x in self.state[i]])
            out += "|"
        return out[:-1]

    def __hash__(self):
        return hash(str(self.state[0] + self.state[1] + self.state[2]))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.state == other.state
        else:
            return False

    def playerwon(self, player):
        for i in range(0,3):
            
            # check each row for a win
            row = self.state[i]
            if row == [player] * 3: return 1
        
            # check each col for a win
            col = [self.state[0][i],self.state[1][i],self.state[2][i]]
            if col == [player] * 3: return 1
        
        # check diagonals
        if self.state[0][0] == player and \
           self.state[1][1] == player and \
           self.state[2][2] == player:
            return 1
    
        if self.state[0][2] == player and \
            self.state[1][1] == player and \
            self.state[2][0] == player:
                return 1
        return 0

    def score(self, player):
        if self.playerwon(player): return 1

        if player == 1:
            if self.playerwon(2): return -1
        else:
            if self.playerwon(1): return -1

        return 0

