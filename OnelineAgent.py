import random

from Agent import Agent

class OnelineAgent(Agent):
    
    def move(self, board):
        
        k = 0
        
        while True:
            k += 1
            
            i = random.randint(0,0)
            if k > 10:i = random.randint(0,1)
            if k > 20:i = random.randint(0,2)
        
            j = random.randint(0,2)

            if board.state[i][j] == 0:
                return (i,j)
    
    def get_name(self):
        return "OnelinerP3ns"
    
    def result(self, oldboard, newboard, reward):
        pass

    def new_game(self):
        pass

    def end_game(self, board, reward):
        pass

